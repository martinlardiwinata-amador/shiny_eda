##### SETTING UP ENVIRONMENT #####

# loading library 
if (!require("pacman")) install.packages("pacman")
pacman::p_load("dplyr", "ggplot2", "plotly", "shiny", 
               "stringr", "rstudioapi", "purrr", "htmltools", 
               "rmarkdown","DT","testthat")

# change directory to source file location 
# setwd(dirname(getActiveDocumentContext()$path))
# setwd("../../")

setwd (here::here())
#setwd("C:/Users/martin.lardiwinata/Documents/Git/Gitlab_a2pg/pmx_eda")
source ("utils.R")
source ("tests/testthat/helper-func.R")

#write.csv (mpg,    "mpg.csv",    row.names = FALSE)
#write.csv (iris,   "iris.csv",   row.names = FALSE)
#write.csv (mtcars, "mtcars.csv", row.names = FALSE)

df <- reading_df ("tests/testthat/CHEMX_STUDY100_PK_20200201V02.csv")
df_filtered <- df %>% filter (MDV == 0)
df1 <- reading_df ("tests/testthat/mpg.csv"   ) # character numeric integer class
df2 <- reading_df ("tests/testthat/iris.csv"  ) # numeric character class
df3 <- reading_df ("tests/testthat/mtcars.csv") # numeric integer class

df1_na <- random_na (df1)
df2_na <- random_na (df2)
df3_na <- random_na (df3)

###################
##### TESTING #####
###################

# profiling_fun - no need for testing
# this function only for profiling

# reading_df
# Older version R (Before R 4.0), stringsAsFactors is very important - Ubuntu use R after 4.00
# should specify stringsAsFactors = FALSE or class can be factor - Ubuntu use R after 4.00
# Assumption that possible class are Integer, Numeric and Character - haven't confirmed yet
# Boolean Class can be a problem (They don't convert 1 and 0 to be Bool, THANKS GOD)
# no need for testing -- very simple

# counting_unique : counting unique value in vector
# possible bug : if column in data frame only contain NA, it counts as 1

test_that("counting_unique", {
  expect_equal(counting_unique(c (1,2,3,3,4,4,4,4)), 4)
  expect_equal(counting_unique(c (1,2,NA)),3) # NA is count as 1 
  expect_equal(counting_unique(c (NA)), 1)
  expect_equal(counting_unique(c ()), 0)
  expect_equal(counting_unique(NA), 1)
  expect_equal(counting_unique("aaa"), 1)
  expect_equal(counting_unique(""), 1)
}) 

# s_unique -- not being used in data 

# colnames_numint : return vector of column name with class numeric or integer
# is.numeric is TRUE for double and integer 
# current setup : default behavior when no numeric -- set some value for future improvement 

test_that("colnames_numint", {
  expect_equal(colnames_numint(df), c ('ID','ATFD','NTFD','ATLD','NTLD','AMT','DOSE','CMT','CONC','LCONC','EVID',
                                       'MDV','BLQ','STUDY','FORM','PTYPE','FOOD','SEXF','RACE','AGE',
                                       'BWT','BSCR','BCRCL','LINE'))
  expect_equal(colnames_numint(df1), c ("displ", "year", "cyl", "cty", "hwy"))
  expect_equal(colnames_numint(df2), c ("Sepal.Length", "Sepal.Width", "Petal.Length", "Petal.Width"))
  expect_equal(colnames_numint(df3), names (df3))
  expect_equal(colnames_numint(df1_na), c ("displ", "year", "cyl", "cty", "hwy"))
  expect_equal(colnames_numint(df2_na), c ("Sepal.Length", "Sepal.Width", "Petal.Length", "Petal.Width"))
  expect_equal(colnames_numint(df3_na), names (df3))
  expect_equal(colnames_numint(data.frame()) %>% length(), 0)
})

# colnames_char : return vector of column name with class character
# current setup : default behavior when no numeric -- set some value for future improvement 
test_that("colnames_char", {
  expect_equal(colnames_char(df), c ('C','USUBJID'))
  expect_equal(colnames_char(df1), c ("manufacturer", "model", "trans", "drv", "fl", "class"))
  expect_equal(colnames_char(df2), c ("Species"))
  expect_equal(colnames_char(df1_na), c ("manufacturer", "model", "trans", "drv", "fl", "class"))
  expect_equal(colnames_char(df2_na), c ("Species"))
  expect_equal(colnames_numint(data.frame()) %>% length(), 0)
})


# colnames_unique : return vector of column name with unique value less than x (default value is 20)
# counting NA as unique value

test_that("colnames_unique", {
  expect_equal(colnames_unique(df, 5), c ("C", "CMT", "EVID", "MDV", "BLQ", "STUDY", "FORM", "PTYPE", "FOOD", "SEXF","RACE"))
  expect_equal(colnames_unique(df1, 10), c ("year", "cyl", "drv", "fl", "class"))
  expect_equal(colnames_unique(df2), c ("Species"))
  expect_equal(colnames_unique(df3), c ("cyl", "vs", "am", "gear", "carb"))
  expect_equal(colnames_unique(df3, less_than = FALSE), c ("mpg", "disp", "hp", "drat", "wt", "qsec"))
})

# filter_dplyr : filtering dataframe based on string/character input 
test_that("filter_dplyr", {
  expect_equal(filter_dplyr(df, "NTLD != 0"), df %>% filter (NTLD !=  0))
  expect_equal(filter_dplyr(df1, "year > 2000"), df1 %>% filter (year > 2000))
  expect_equal(filter_dplyr(df1, "year > 2000; cyl == 4"), df1 %>% filter (year > 2000, cyl == 4))
  expect_equal(filter_dplyr(df2, "Petal.Length > 1.4"), df2 %>% filter (Petal.Length > 1.4))
  expect_equal(filter_dplyr(df3, "cyl == 6"), df3 %>% filter (cyl == 6))
})

# count_na : counting number of NA

test_that("count_na", {
  expect_equal (count_na(df, "AMT"),1280)
  expect_equal(count_na(df1, "cyl"),0)
  expect_equal(count_na(df2, "Petal.Width"),0)
  expect_equal(count_na(df3, "disp"),0)
  expect_equal(count_na(df1_na, "cyl"),2)
  expect_equal(count_na(df2_na, "Petal.Width"),2)
  expect_equal(count_na(df3_na, "disp"),2)
})


# GM 
test_that("GM", {
  expect_equal (GM(c(10,15,21,22)) %>% round (2), 16.22) # have been checked in website : 
                                                         # http://www.alcula.com/calculators/statistics/geometric-mean/
  expect_equal (GM(c(10,15,21,22,NA,NA)) %>% round (2), 16.22) # should ignore NA
  expect_equal (GM(c(10,15,21,22,-105)) %>% round (2), 16.22) # should ignore negative

})

########################
##### PLOT TESTING #####
########################

# point_line_plot : point and line plot
# convert color to as.character(color) if is.character(color) == False -- effect ggplot labelling
# convert facet wrap to as.formula
# using aes_string -- I think it's better to use aes (!!sym()) -- read somewhere
# df is used for coloring -- if unique(color) < 9 use color_brewer
# na.rm = False for stat summary (median, mean, geom) -- not a problem, already checked

# box_plot_func : box_plot
# convert ALL x and fill columns to as.character()
# using aes (!!sym ())
# df and id_col argument not being used
# calling stat_box_data function

# hist_plot_fun : histogram
# using aes_string 
# df argument not being used
# no y value

# ggplot_only_fun : filtering and plotting ggplot
# filter id_col -- id_col == "NA" for not being filtered
# filter NA in x and y column (after filter id_col - can be a problem ?)
# handling if column being convert (as.character ())
# didn't filter NA in x column if as.character (x) -- x column box plot not being filter

# ggplot_and_plotly
# calling ggplot_only_fun function 

# design improvement :
# create function to filter id_col and NA
# create function for coloring : so no need to pass df in point_line_plot
# df only being pass in ggplot and plotly function (and coloring function)

# point_line_plot
li_point <- point_line_plot(df = df, x_col = "NTLD", y_col = "CONC", x_lab= "label xx", y_lab= "label yy", 
                            title_lab = "title title", plot_type= c ("p_plot", "l_plot"), color_col = "DOSE", 
                            group_col = "SEXF", lines_type= "geom", log_x= FALSE, log_y = FALSE, 
                            limit_x= c (0,100), limit_y= c(0,2300))

test_that("li_point", {
  expect_equal (li_point$aes_fun$x %>% as_label () %>% remove_aschar (),"NTLD")
  expect_equal (li_point$aes_fun$y %>% as_label () %>% remove_aschar (),"CONC")
  expect_equal (li_point[[2]]$colour%>% as_label () %>% remove_aschar (),"DOSE")
})

# point_line_plot and ggplot_only_fun
# test 1
li_p1 <- point_line_plot(df = df, x_col = "NTLD", y_col = "CONC", x_lab= "label xx", y_lab= "label yy", title_lab = "title title", 
                         plot_type= c ("p_plot", "l_plot"), color_col = "DOSE", group_col = "SEXF", lines_type= "geom", 
                         log_x= FALSE, log_y = FALSE, limit_x= c (0,100), limit_y= c(0,2300))
plot1 <- ggplot_only_fun (df, li_p1)
info1 <- gg_layer_info_plot (plot1)

# test 2 - different data
li_p2 <- point_line_plot(df = df1, x_col = "cyl", y_col = "hwy", x_lab= "label xx", y_lab= "label yy", title_lab = "title title", 
                         plot_type= c ("p_plot", "l_plot"), color_col = "class", group_col = "NA", lines_type= "median", 
                         log_x= FALSE, log_y = FALSE, limit_x= c (3,8), limit_y= c(1,50))
plot2 <- ggplot_only_fun (df1,li_p2)

info2 <- gg_layer_info_plot (plot2)

# test 3 - only plot (not line and plot) - SEXF, ATLD and LCONC
li_p3 <- point_line_plot(df = df, x_col = "ATLD", y_col = "LCONC", x_lab= "label xx", y_lab= "label yy", title_lab = "title title", 
                         plot_type= c ("p_plot"), color_col = "SEXF", group_col = "NA", lines_type= "geom", 
                         log_x= FALSE, log_y = FALSE, limit_x= c (0,100), limit_y= c(0,2300))
plot3 <- ggplot_only_fun (df, li_p3)
info3 <- gg_layer_info_plot (plot3)

# ggplot_only_fun with id_col; using df_filtered
li_p4 <- point_line_plot(df_filtered, x_col = "NTLD", y_col = "CONC", x_lab= "label xx", y_lab= "label yy", title_lab = "title title", 
                         plot_type= c ("p_plot", "l_plot"), color_col = "DOSE", group_col = "RACE", lines_type= "mean", 
                         log_x= FALSE, log_y = TRUE, limit_x= c (0,100), limit_y= c(0,2300))
plot4 <- ggplot_only_fun (df_filtered, li_p4, id_col = "ID")
info4 <- gg_layer_info_plot (plot4)

test_that("point_line_plot and ggplot_only_fun", {
  expect_equal (info1, list (x = "NTLD",
                             y = "CONC",
                             color = "DOSE",
                             facet = "SEXF",
                             df = df %>% filter (!is.na (NTLD)) %>% filter (!is.na (CONC)),
                             line_func = "geom"
  ))
  expect_equal (info2, list (x = "cyl",
                             y = "hwy",
                             color = "class",
                             facet = "NA",
                             df = df1 %>% filter (!is.na (cyl)) %>% filter (!is.na (hwy)),
                             line_func = "median"
  ))
  
  expect_equal (info3, list (x = "ATLD",
                             y = "LCONC",
                             color = "SEXF",
                             facet = "NA",
                             df = df %>% filter (!is.na (ATLD)) %>% filter (!is.na (LCONC)),
                             line_func = "NA"
  ))
  expect_equal (info4, list (x = "NTLD",
                             y = "CONC",
                             color = "DOSE",
                             facet = "RACE",
                             df = df_filtered %>% filter (!duplicated (ID))%>%filter (!is.na (ATLD)) %>% filter (!is.na (LCONC)),
                             line_func = "mean"
  ))
})


# stat_box_data
# expect no NA in vector
test_that("stat_box_data", {
  expect_equal (stat_box_data(c(1,2,3,4,5)), data.frame(y = 5.25, label = "n = 5 \n median = 3 \n"))
})

# box_plot_func 
li_box <- box_plot_func (df = df1, x_col = "cyl", y_col = "hwy", id_col = "NA", 
                         x_lab = "", y_lab = "", title_lab= "", text_desc = TRUE)

test_that("li_box", {
  expect_equal (li_box$aes_fun$x %>% as_label () %>% remove_aschar (), "cyl")
  expect_equal (li_box$aes_fun$y %>% as_label () %>% remove_aschar (), "hwy")
  expect_equal (li_box$aes_fun$fill %>% as_label () %>% remove_aschar (),"cyl")
})

# box_plot_func and ggplot
# don't filter NA in x column 

bli_p_1 <- box_plot_func (df = df1, x_col = "cyl", y_col = "hwy", id_col = "NA", 
                          x_lab = "", y_lab = "", title_lab= "", text_desc = TRUE)

bplot_1 <- ggplot_only_fun (df1,bli_p_1)
binfo_1 <- gg_layer_info_box (bplot_1)

# different df
bli_p_2 <- box_plot_func (df = df2, x_col = "Species", y_col = "Petal.Length", id_col = "NA", 
                          x_lab = "", y_lab = "", title_lab= "", text_desc = TRUE)
bplot_2 <- ggplot_only_fun (df2,bli_p_2)
binfo_2 <- gg_layer_info_box (bplot_2)

# with id_col
bli_p_3 <- box_plot_func (df = df, x_col = "SEXF", y_col = "BCRCL", id_col = "NA", 
                          x_lab = "", y_lab = "", title_lab= "", text_desc = TRUE)
bplot_3 <- ggplot_only_fun (df,bli_p_3, id_col = "USUBJID")
binfo_3 <- gg_layer_info_box (bplot_3)

# with NA
bli_p_4 <- box_plot_func (df = df2_na, x_col = "Species", y_col = "Petal.Length", id_col = "NA", 
                          x_lab = "", y_lab = "", title_lab= "", text_desc = TRUE)
bplot_4 <- ggplot_only_fun (df2_na,bli_p_4)
binfo_4 <- gg_layer_info_box (bplot_4)


test_that("box_plot_func and ggplot", {
  expect_equal (binfo_1, list (x = "cyl",
                               y = "hwy",
                               df = df1 %>% filter (!is.na (hwy))
  ))
  expect_equal (binfo_2, list (x = "Species",
                               y = "Petal.Length",
                               df = df2 %>% filter (!is.na (Petal.Length))
  ))
  expect_equal (binfo_3, list (x = "SEXF",
                               y = "BCRCL",
                               df = df %>% filter (! duplicated (USUBJID))%>% filter (!is.na (BCRCL))
  ))
  expect_equal (binfo_4, list (x = "Species",
                               y = "Petal.Length",
                               df = df2_na %>% filter (!is.na (Petal.Length))
  ))
})

# check median and number of observation 
ggplotly (bplot_1)
ggplotly (bplot_2)
ggplotly (bplot_3)
ggplotly (bplot_4)

df1 %>% filter (!is.na (hwy)) %>% 
  group_by (cyl) %>% summarise (n = n(), median = median (hwy))

df2 %>% filter (!is.na (Petal.Length)) %>% 
  group_by (Species) %>% summarise (n = n(), median = median (Petal.Length))

df %>% filter (! duplicated (USUBJID))%>% filter (!is.na (BCRCL)) %>% 
  group_by (SEXF) %>% summarise (n = n(), median = median (BCRCL))

df2_na %>% filter (!is.na (Petal.Length)) %>% 
  group_by (Species) %>% summarise (n = n(), median = median (Petal.Length))

# hist_plot_func
li_hist <- hist_plot_func (df = df1, x_col = "hwy", x_lab = "", y_lab = "", title_lab = "")

test_that("li_hist", {
  expect_equal (li_hist$aes_fun$x %>% as_label () %>% remove_aschar () , "hwy")
})

# hist_plot_func and ggplot function 
hli_p_1 <- hist_plot_func (df = df1, x_col = "hwy", x_lab = "", y_lab = "", title_lab = "")
hplot_1 <- ggplot_only_fun (df1, hli_p_1)
hinfo_1 <- gg_layer_info_hist (hplot_1)

# with NA
hli_p_2 <- hist_plot_func (df = df1_na, x_col = "hwy", x_lab = "", y_lab = "", title_lab = "")
hplot_2 <- ggplot_only_fun (df1_na, hli_p_2)
hinfo_2 <- gg_layer_info_hist (hplot_2)

test_that("hist_plot_func", {
  expect_equal(hinfo_1, list (x = "hwy",
                              df = df1 %>% filter (!is.na(hwy))))
  expect_equal(hinfo_2, list (x = "hwy",
                              df = df1_na %>% filter (!is.na(hwy))))
})

# ggplot_and_plotly
# plotting plotly - no need unit test

# ggplot_only_fun
# tested with other function

# plotting_with_li_res
# combination of filter_dplyr and ggplot_only_fun function 


##########################
##### STRING TESTING #####
##########################

# warning_message
df[["CONC"]] %>% is.na () %>% sum () # 361
df[["AMT"]] %>% is.na () %>% sum () # 1280

(df %>% filter (MDV == 0))[["CONC"]] %>% is.na () %>% sum () # 0
(df%>% filter (MDV == 0))[["AMT"]] %>% is.na () %>% sum () # 999

test_that("warning_message", {
  expect_equal(warning_message (df,"", "CONC", "AMT"),
               'There are no filter applied.\nThere are 361 NA in column CONC\nThere are 1280 NA in column AMT')
  expect_equal(warning_message (df %>% filter (MDV == 0),"MDV == 0", "CONC", "AMT"),
               'Data is filtered by : MDV == 0\nThere are 999 NA in column AMT')
})

# description_fun
# description for report
# blank : id_col is "NA" and filtered is "" 

desc1 <- list (filtered_1 = "MDV == 0",
               id_col_1 = "NMID",
               filtered_2 = "",
               id_col_2 = "NMID",
               filtered_3 = "MDV == 0",
               id_col_3 = "NA",
               filtered_4 = "",
               id_col_4 = "NMID"
)

res_desc1 <- description_fun (desc1,4)

test_that("description_fun", {
  expect_equal(res_desc1$txt_1 , "Data is filtered by : MDV == 0 and !duplicated (NMID) in plot 1")
  expect_equal(res_desc1$txt_2 ,"Data is filtered by : !duplicated (NMID) in plot 2, 4")
  expect_equal(res_desc1$txt_3 , "Data is filtered by : MDV == 0 in plot 3")
  
})


# combine_text 
test_that("combine_text", {
  expect_equal(combine_text ("MDV == 0", "!duplicated (NMID)"),'MDV == 0 ; !duplicated (NMID)')
  expect_equal(combine_text ("", "!duplicated (NMID)"),'!duplicated (NMID)')
  expect_equal(combine_text ("MDV == 0", ""),'MDV == 0')
  expect_equal(combine_text ("", ""),'')
})


#########################
##### TABLE TESTING #####
#########################

# id_obs_func : for categorical table summary function
# count NA as unique ID !!!! - is this what user want ??
# check percentage in NID !!! - is this what user want ??
# will create own NA Grouping - not a problem

df1_temp_table <- id_obs_func (df1, c("class","cyl"), "manufacturer")

df1_temp <- df1 %>% filter (class == "subcompact", cyl ==4 ) %>% 
  summarise (n = n(), n_distinct = n_distinct(manufacturer)) 

(df1_temp [1,1] * 100 / nrow (df1)) %>% round (2) # percentage of NOBS : 8.97 passed test 
(df1_temp [1,2] * 100 / length (unique (df1[["manufacturer"]]))) %>% round (2) #  # percentage of NOBS : 26.67 passed test


# table_continuous
# part of table_continuous_multi_target 
# unit test are in table_continuous_multi_target

# table_continuous_multi_target
# NA.rm is TRUE for everything except Missing and N !!!! is that what user want ?
table_continuous_multi_target (df1, c ("cty", "hwy"), c("manufacturer"), "NA")
table_continuous_multi_target (df1_na, c ("cty", "hwy"), c("manufacturer"), "NA")
df1_temp <- df1 %>% filter (manufacturer == "audi" ) %>% summary ()
df1_temp2 <- df1_na %>% filter (manufacturer == "audi" ) %>% summary ()

