# creating NA in dataset : NA is diagonal from top and bottom
# each column have 2 NA (top and bottom)
random_na <- function (df){
  for (i in 1 : (length (df)*2)) {
    if (i <= length (df)) {df [i,i] <- NA}
    else {df[nrow (df) - i,i-length(df)] <- NA}
  }
  return (df)
}

remove_aschar <- function (x){
  result <- trimws (x, "both") # trim whitespace before and after
  if (str_detect (result , regex ("^as.character"))){ # if start with as.character
    result <- gsub ("as.character", "", result) %>% trimws("both")
    result <- result %>% substr (2,nchar(result)-1) 
  }
  return (result)
}

# point and line plot
gg_layer_info_plot <- function (plot){
  p <- ggplot_build(plot)
  li <- list (x = p$plot$mapping$x %>% as_label () %>% remove_aschar(),
              y = p$plot$mapping$y %>% as_label () %>% remove_aschar(),
              color = p$plot$mapping$colour %>% as_label () %>% remove_aschar(),
              facet = p$plot$facet$params$facets %>% names (),
              df = p$plot$data)
  
  if (is.null (li[["facet"]])){li[["facet"]] <- "NA"}
  
  # line_func : median, mean, geom
  # if stat_param / Lines exist
  for (layer in p$plot$layers){
    if (is.null(layer$stat_params$fun)){next} # skip if not lines
    li[["line_func"]] = (p$plot$layer[[2]]$stat_params$fun %>% body () %>% as.character ())[2]
  }
  
  # if line didn't exist
  if (!("line_func" %in% (li %>% names()))) {li[["line_func"]] <- "NA"}
  if (li[["line_func"]] == "exp(mean(log(x[which(x > 0)])))"){li[["line_func"]] <- "geom"} # line = geom
  
  return (li)
}

# for box plot
gg_layer_info_box <- function (plot){
  p <- ggplot_build(plot)
  li <- list (x = p$plot$mapping$x %>% as_label () %>% remove_aschar(),
              y = p$plot$mapping$y %>% as_label () %>% remove_aschar(),
              df = p$plot$data)
  
  return (li)
}

# for histogram plot
gg_layer_info_hist <- function (plot){
  p <- ggplot_build(plot)
  li <- list (x = p$plot$mapping$x %>% as_label () %>% remove_aschar(),
              df = p$plot$data)
  
  return (li)
}
